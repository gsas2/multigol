const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const common = require('./webpack.common');
const { description, version } = require('./package.json');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    host: '0.0.0.0',
    proxy: {
      '/socket.io/*': {
        target: 'ws://localhost:8082',
        ws: true
      }
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: `[DEV] ${description} [${version}]`
    })
  ]
});
