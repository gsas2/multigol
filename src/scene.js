import { Scene, FacebookInstantGamesLeaderboard } from 'phaser';
import io from 'socket.io-client';

import shipAsset from './assets/spaceShips_001.png';
import enemyAsset from './assets/enemyBlack5.png';
import ballAsset from './assets/ball.png';
import playerAsset from './assets/player.png';

const MY_TEAM_TINT = 0x0000ff;
const OPPOSITE_TEAM_TINT = 0xdd0000;
const PLAYER_X_DELTA = 100;

class Scene1 extends Scene {
  constructor() {
    super({ key: 'Scene1' });
  }

  preload() {
    this.load.image('ship', shipAsset);
    this.load.image('otherPlayer', enemyAsset);
    this.load.spritesheet(
      'ball',
      ballAsset,
      {
        frameWidth: 32,
        frameHeight: 32
      }
    );
    this.load.spritesheet(
      'player',
      playerAsset,
      {
        frameWidth: 58,
        frameHeight: 18
      }
    );
    this.game.registry.events._events.blur = [];
    this.game.registry.events._events.focus = [];
    this.game.registry.events._events.hidden = [];
    this.game.onBlur                        = () => console.log("blur");
    this.game.onFocus                       = () => console.log("focus");
    this.game.onPause                       = () => console.log("pause");
    this.focusLoss                     = () => console.log("focusloss");
    this.focusGain                     = () => console.log("focusgain");    
  }

  create() {
    this.physics.world.setBounds(
      0,
      0,
      this.sys.game.config.width,
      this.sys.game.config.height,
      false,
      false,
      true,
      true
    );

    this.controlledSticks = {};
    this.lastKicker = null;
    this.cursors = this.input.keyboard.createCursorKeys();

    this.socket = io();
    this.socket.on('currentGamers', (gamers) => {
      console.log('currentGamers', gamers);
      Object.keys(gamers).forEach(gamerId => {
        if (gamers[gamerId].gamerId === this.socket.id) {
          this.controlledSticks = gamers[gamerId].controlledSticks;
          this.teamSide = gamers[gamerId].teamSide;
          this.setupField();
        }
      });
    });

    this.socket.on('playerMoved', (playerInfo) => {
      this.updatePlayerPosition(playerInfo);
    });

    this.socket.on('ballKicked', ({ lastKicker,  ballInfo }) => {
      console.log('ballKicked', lastKicker.name, ballInfo);
      this.updateBall(this.getSanitizedBallProps(ballInfo.position, ballInfo.velocity));
    });

    this.socket.on('throwTheBall', ({ position, velocity, scores }) => {
      this.lastKicker = null;
      this.goalDetected = false;
      this.scores[0].setText(scores[0]);
      this.scores[1].setText(scores[1]);
      this.updateBall(this.getSanitizedBallProps(position, velocity));
    });

    this.socket.on('lastKicker', ({ name }) => {
      console.log('lastKicker', name);
      this.lastKicker = {
        name,
        controlledByMe: false
      };
    });

    this.socket.on('goalConfirmed', ({ goalForTeam, scores }) => {
      this.physics.pause();
      this.goalMessage.setTint(goalForTeam ? MY_TEAM_TINT : OPPOSITE_TEAM_TINT).setVisible(true);
      setTimeout(() => {
        this.physics.resume();
        this.goalMessage.setVisible(false);
      }, 4000);

      this.scores[0].setText(scores[0]);
      this.scores[1].setText(scores[1]);
      console.log('Goal Confirmed!!!', goalForTeam, scores);
    });

  }

  getSanitizedBallProps(position, velocity) {
    let fixedPosition = position;
    let fixedVelocity = velocity;
    if (this.teamSide === 1) {
      fixedPosition = this.transformToMirrorPosition(position);
      fixedVelocity = this.transformToMirrorVelocity(velocity);
    }

    return {
      position: fixedPosition,
      velocity: fixedVelocity
    }
  }

  updateBall({ position, velocity }) {
    this.ball.setPosition(position.x, position.y)
      .setVelocity(velocity.x, velocity.y);
  }

  updatePlayerPosition({ playerId, x, y, frame }) {
    let finish = false;
    if (this.myTeam) {
      Object.keys(this.myTeam).forEach(fieldPosition => {
        if (this.controlledSticks[fieldPosition]) {
          return;
        }
        const teamLine = this.myTeam[fieldPosition];
        const players = teamLine.getChildren();
        const playerToUpdate = players.find(player => player.name === playerId);
        if (playerToUpdate) {
          playerToUpdate.x = x;
          playerToUpdate.y = y;
          playerToUpdate.skin.y = y;
          playerToUpdate.skin.setFrame(frame);
          finish = true;
          return;
        }
      });
      if (finish) {
        return;
      }
    }
    if (this.oppositeTeam) {
      // We should transform / mirror X, Y and Frame values
      Object.keys(this.oppositeTeam).forEach(fieldPosition => {
        const teamLine = this.oppositeTeam[fieldPosition];
        const players = teamLine.getChildren();
        const playerToUpdate = players.find(player => player.name === playerId);
        if (playerToUpdate) {
          const mirroredValues = this.transformToMirrorPosition({ x, y });
          playerToUpdate.x = mirroredValues.x;
          playerToUpdate.y = mirroredValues.y;
          playerToUpdate.skin.y = mirroredValues.y;
          playerToUpdate.skin.setFrame(frame);
          finish = true;
          return;
        }
      });
    }
  }

  transformToMirrorPosition({ x, y }) {
    const mirrorX = this.sys.game.config.width - x;
    const mirrorY = this.sys.game.config.height - y;

    return {
      x: mirrorX,
      y: mirrorY
    }
  }

  transformToMirrorVelocity({ x, y }) {
    return {
      x: -x,
      y: -y
    }
  }  

  update() {
    const MOVEMENT_ACCELERATION = 5000;
    if (this.myTeam) {
      Object.keys(this.myTeam).forEach(fieldPosition => {
        if (!this.controlledSticks[fieldPosition]) {
          return;
        }
        const teamLine = this.myTeam[fieldPosition];
        const players = teamLine.getChildren();      
        if (this.cursors.up.isDown) {
          if (players[0].y - players[0].height * 2 > 0) {
            teamLine.setVelocityY(-500);
          } else {
            teamLine.setVelocityY(0);
          }
        } else if (this.cursors.down.isDown) {
          if (players[players.length - 1].y + players[0].height * 2 < this.sys.game.config.height) {
            teamLine.setVelocityY(500);
          } else {
            teamLine.setVelocityY(0);
          }
        } else {
          teamLine.setVelocityY(0);
        }

        players.forEach(player => {
          player.skin.y = player.y;
          if (player.x <= player.stillPosition.x - PLAYER_X_DELTA) {
            player.body.stop();
            player.x = player.stillPosition.x - PLAYER_X_DELTA;
            player.position = -4;
          }
          if (player.x >= player.stillPosition.x + PLAYER_X_DELTA) {
            player.body.stop();
            player.x = player.stillPosition.x + PLAYER_X_DELTA;
            player.position = 4;
          }
  
          // if (this.cursors.left.isDown && this.cursors.right.isDown) {
          //   player.body.setVelocityX(0);
          //   player.position = 0;
          //   player.x = player.stillPosition.x;
          // } else 
          if (this.cursors.left.isDown) {
            if (player.position > -4) {
              player.position = player.position - 1;
              this.physics.accelerateTo(
                player,
                player.stillPosition.x - PLAYER_X_DELTA,
                player.y,
                MOVEMENT_ACCELERATION
              );
            }
          } else if (this.cursors.right.isDown) {
            if (player.position < 4) {
              player.position = player.position + 1;
              this.physics.accelerateTo(
                player,
                player.stillPosition.x + PLAYER_X_DELTA,
                player.y,
                MOVEMENT_ACCELERATION
              );
            }
          } else {
            const ERROR_DELTA = 1;
            if (player.x > player.stillPosition.x + ERROR_DELTA) {
              this.physics.accelerateTo(player, player.stillPosition.x, player.y, 120);
              player.position = 0;
            } else if (player.x < player.stillPosition.x - ERROR_DELTA) {
              this.physics.accelerateTo(player, player.stillPosition.x, player.y, 120);
              player.position = 0;
            } else {
              player.body.setVelocityX(0);
              player.position = 0;
              player.x = player.stillPosition.x;
            }
          }
          const currentPosition = player.x - player.stillPosition.x;
          const step = PLAYER_X_DELTA / 4;
          let currentFrame = 4;
  
          if (currentPosition > PLAYER_X_DELTA - step * 1) {
            currentFrame = 0;
          } else if (currentPosition > PLAYER_X_DELTA - step * 2) {
            currentFrame = 1;
          } else if (currentPosition > PLAYER_X_DELTA - step * 3) {
            currentFrame = 2;
          } else if (currentPosition > PLAYER_X_DELTA - step * 4) {
            currentFrame = 3;
          } else if (currentPosition < - PLAYER_X_DELTA + step * 1) {
            currentFrame = 8;
          } else if (currentPosition < - PLAYER_X_DELTA + step * 2) {
            currentFrame = 7;
          } else if (currentPosition < - PLAYER_X_DELTA + step * 3) {
            currentFrame = 6;
          } else if (currentPosition < - PLAYER_X_DELTA + step * 2) {
            currentFrame = 5;
          }
          if ([0, 1, 7, 8].includes(currentFrame)) {
            player.body.checkCollision.none = true;
          } else {
            player.body.checkCollision.none = false;
          }
          player.skin.setFrame(currentFrame);

          // emit player movement
          const x = player.x;
          const y = player.y;
          const frame = currentFrame;
          if (player.oldPosition && (
              x !== player.oldPosition.x ||
              y !== player.oldPosition.y ||
              frame !== player.oldPosition.frame
            )) {
            this.socket.emit('playerMovement', {
              playerId: player.name,
              x,
              y,
              frame
            });
          }
          // save old position data
          player.oldPosition = {
            x,
            y,
            frame
          };
        });
      })
    }

    if (this.ball) {
      if (this.ball.body.velocity.x > 0 || this.ball.body.velocity.y > 0) {
        this.ball.anims.play('forward', true);
      } else if (this.ball.body.velocity.x < 0 || this.ball.body.velocity.y < 0) {
        this.ball.anims.play('backward', true);
      } else {
        this.ball.anims.stop();
      }

      if (this.lastKicker && this.lastKicker.controlledByMe) {
        const ballInfo = this.getSanitizedBallProps(
          {
            x: this.ball.x,
            y: this.ball.y
          }, {
            x: this.ball.body.velocity.x,
            y: this.ball.body.velocity.y
          }
        );

        this.socket.emit('ballUpdate', {
          lastKicker: {
            name: this.lastKicker.name
          },
          ballInfo
        });

        if (this.ball.body.velocity.x === 0 && this.ball.body.velocity.y === 0) {
          this.lastKicker = null;
        }

        if (!this.goalDetected) {
          const isGoalOppositeTeam = this.ball.x < 0;
          const isGoalMyTeam = this.ball.x > this.sys.game.config.width;
          if (isGoalMyTeam || isGoalOppositeTeam) {
            this.goalDetected = true;
            console.log('Goal Emitted!!!!');
            this.socket.emit('goalDetected', {
              myTeamSide: this.teamSide,
              goalForMyTeam: isGoalMyTeam
            });
          }
        }
      }
    }
  }

  createTeamPlayer(teamColour, x, y, oppositeTeam = false, playerId) {
    const TEAM_PLAYER_FOOT_SHAPE = [4, 25.3, 5.3]; // Circle
    const SCALE_FACTOR = 4;

    const teamPlayer = this.physics.add.image(
      x,
      y,
      'player'
    );
    teamPlayer.skin = this.add.sprite(
      x,
      y,
      'player'
    );
    teamPlayer.setScale(SCALE_FACTOR);
    teamPlayer.skin.setScale(SCALE_FACTOR);
    teamPlayer.skin.setTint(teamColour);
    teamPlayer.skin.setFrame(4);
    teamPlayer.skin.setDepth(10);
    teamPlayer.stillPosition = teamPlayer.getCenter();
    teamPlayer.body.setImmovable(true);
    teamPlayer.setVisible(false);
    teamPlayer.setCircle(...TEAM_PLAYER_FOOT_SHAPE);
    teamPlayer.setCollideWorldBounds(true);
    teamPlayer.setMass(20);
    teamPlayer.setName(playerId);
    if (oppositeTeam) {
      teamPlayer.setRotation(Math.PI);
      teamPlayer.skin.setRotation(Math.PI);
    }

    return teamPlayer;
  }

  createTeam(sideId, teamColour, oppositeTeam = false) {
    const team = {
      goalkeeper: this.physics.add.group(),
      defense: this.physics.add.group(),
      middle: this.physics.add.group(),
      attack: this.physics.add.group()
    };
    const goalkeeperX = oppositeTeam ? this.sys.game.config.width - 50 : 50;
    const defenseX = oppositeTeam ? this.sys.game.config.width - 300 : 300;
    const middleX = oppositeTeam ? this.sys.game.config.width - 800 : 800;
    const attackX = oppositeTeam ? this.sys.game.config.width - 1300 : 1300;
    const widthMiddle = (this.sys.game.config.height / 2);
    const defenseSeparation = 300;
    const middleSeparation = 200;
    const attackSeparation = 300;
    const teamConfig = {
      goalkeeper: [{
        x: goalkeeperX,
        y: widthMiddle
      }],
      defense: [
        {
          x: defenseX,
          y: widthMiddle - defenseSeparation / 2
        },
        {
          x: defenseX,
          y: widthMiddle + defenseSeparation / 2
        }
      ],
      middle: [
        {
          x: middleX,
          y: widthMiddle - middleSeparation * 2
        },
        {
          x: middleX,
          y: widthMiddle - middleSeparation
        },
        {
          x: middleX,
          y: widthMiddle
        },
        {
          x: middleX,
          y: widthMiddle + middleSeparation
        },
        {
          x: middleX,
          y: widthMiddle + middleSeparation * 2
        }
      ],
      attack: [
        {
          x: attackX,
          y: widthMiddle - attackSeparation
        },
        {
          x: attackX,
          y: widthMiddle
        },
        {
          x: attackX,
          y: widthMiddle + attackSeparation
        }
      ]
    };
    let playerNumber = 1;
    Object.keys(teamConfig).forEach(fieldPosition => {
      teamConfig[fieldPosition].forEach(player => {
        let playerId = `${fieldPosition}-${playerNumber}`;
        if (!oppositeTeam) {
          playerId = `${sideId}-${playerId}`;
        } else if (sideId === 0) {
          playerId = `1-${playerId}`;
        } else {
          playerId = `0-${playerId}`;
        }
        const teamPlayer = this.createTeamPlayer(
          teamColour,
          player.x,
          player.y,
          oppositeTeam,
          playerId,
        );
        teamPlayer.controlledByMe = !oppositeTeam && this.controlledSticks[fieldPosition];
        team[fieldPosition].add(teamPlayer);
        playerNumber++;
      });
      this.physics.add.collider(team[fieldPosition], this.ball, (ball, player) => {
        if (player.controlledByMe) {
          console.log('La toca ', player.name);        
          this.lastKicker = player;
          this.socket.emit('lastKicker', {
            name: player.name
          });
        }
      });
    });

    return team;
  }

  drawFieldLines() {
    const graphics = this.add.graphics({
      lineStyle: {
        width: 12,
        color: 0xaaaaaa,
        alpha: 1
      }
    });
    const middleY = this.sys.game.config.height / 2;
    const middleX = this.sys.game.config.width / 2;
    const goalArea1 = this.add.rectangle(0, middleY, 220, 450);
    const goalArea2 = this.add.rectangle(this.sys.game.config.width, middleY, 220, 450);
    const penaltyArea1 = this.add.rectangle(0, middleY, 380, 670);
    const penaltyArea2 = this.add.rectangle(this.sys.game.config.width, middleY, 380, 670);
    const centralCircle = this.add.circle(middleX, middleY, 150);
    const middleLine = this.add.line(
      0,
      0,
      middleX,
      0,
      middleX,
      this.sys.game.config.height,
      0xaaaaaa,
      1
    ).setOrigin(0, 0).setLineWidth(6, 6);

    const cornerLines = this.physics.add.staticGroup();
    const cornerLine1 = this.add.line(0, 0, 1, 0, 1, middleY - 215, 0xaaaaaa, 0)
      .setOrigin(0, 0)
      .setLineWidth(6, 6)
      .setSize(5, middleY - 215);
    cornerLines.add(cornerLine1);

    const cornerLine2 = this.add.line(0, middleY + 215, 1, middleY + 215, 1, this.sys.game.config.height, 0xaaaaaa, 0)
      .setOrigin(0, 0)
      .setLineWidth(6, 6)
      .setSize(5, middleY - 215);
    cornerLines.add(cornerLine2);

    const cornerLine3 = this.add.line(
      this.sys.game.config.width - 2,
      0,
      this.sys.game.config.width - 2,
      0,
      this.sys.game.config.width - 2,
      middleY - 215,
      0xaaaaaa,
      0
      ).setOrigin(0, 0)
      .setLineWidth(6, 6)
      .setSize(5, middleY - 215);
    cornerLines.add(cornerLine3);

    const cornerLine4 = this.add.line(
      this.sys.game.config.width - 2,
      middleY + 215,
      this.sys.game.config.width - 2,
      middleY + 215,
      this.sys.game.config.width - 2,
      this.sys.game.config.height,
      0xaaaaaa,
      0
      ).setOrigin(0, 0)
      .setLineWidth(6, 6)
      .setSize(5, middleY - 215);
    cornerLines.add(cornerLine4);

    this.physics.add.collider(cornerLines, this.ball);

    graphics.strokeCircleShape(centralCircle);
    graphics.strokeLineShape(middleLine);
    
    graphics.strokeRectShape(goalArea1.getBounds());
    graphics.strokeRectShape(goalArea2.getBounds());
    graphics.strokeRectShape(penaltyArea1.getBounds());
    graphics.strokeRectShape(penaltyArea2.getBounds());
  }

  setupField() {
    this.addBall();
    this.drawFieldLines();

    this.myTeam = this.createTeam(this.teamSide, this.teamSide ? MY_TEAM_TINT : OPPOSITE_TEAM_TINT);
    this.oppositeTeam = this.createTeam(this.teamSide, this.teamSide ? OPPOSITE_TEAM_TINT : MY_TEAM_TINT, true);

    this.add.text(18, 14, ' - ', { fontSize: '48px', fill: 'white' }),

    this.scores = [
      this.add.text(16, 16, '0', { fontSize: '48px', fill: 'red' }),
      this.add.text(80, 16, '0', { fontSize: '48px', fill: 'blue' })
    ];

    this.goalMessage = this.add.text(
      this.sys.game.config.width / 2 - 400,
      this.sys.game.config.height / 2 - 60,
      "GOOOOLLLLL!!!",
      {
        fontSize: '120px',
        fill: 'white'
      }
    ).setDepth(100).setVisible(false);
  }

  addBall() {
    const middleY = this.sys.game.config.height / 2;
    const middleX = this.sys.game.config.width / 2;

    this.ball = this.physics.add.sprite(
      middleX,
      middleY,
      'ball'
    ).setScale(2);

    this.anims.create({
      key: 'forward',
      frames: this.anims.generateFrameNumbers('ball', { start: 0, end: 3 }),
      frameRate: 10,
      repeat: -1
    });

    this.anims.create({
      key: 'backward',
      frames: this.anims.generateFrameNumbers('ball', { start: 3, end: 0 }),
      frameRate: 10,
      repeat: -1
    });
    this.ball.setMass(10);
    this.ball.setCircle(16, 0, 0);
    this.ball.setBounce(0.8);
    this.ball.setCollideWorldBounds(true);
    this.ball.setDrag(80);
    this.ball.setAngularDrag(80);
    this.ball.setDepth(5);
  }
};

export default Scene1;
