import { Scene } from 'phaser';
import io from 'socket.io-client';

class PreloadScene extends Scene {
  constructor() {
    super({ key: 'PreloadScene' });
  }

  preload() {
    this.game.registry.events._events.blur = [];
    this.game.registry.events._events.focus = [];
    this.game.registry.events._events.hidden = [];
    this.game.onBlur = () => console.log("blur");
    this.game.onFocus = () => console.log("focus");
    this.game.onPause = () => console.log("pause");
    this.focusLoss = () => console.log("focusloss");
    this.focusGain = () => console.log("focusgain");    
  }

  create() {
    const styles = {
      color: '#ffffff',
      align: 'center',
      fontSize: 52
    };

    let texts = [];

    texts.push(
      this.add.text(
        this.sys.game.config.width / 2 - 200,
        this.sys.game.config.height / 2 - 60,
        'Conectando...',
        styles
      ).setOrigin(0, 0)
    );

    const socket = io({
      transports: ['websocket']
    });
    
    // on reconnection, reset the transports option, as the Websocket
    // connection may have failed (caused by proxy, firewall, browser, ...)
    socket.on('reconnect_attempt', () => {
      socket.io.opts.transports = ['polling', 'websocket'];
    });

    socket.on('connect', () => {
      console.log("You're connected to socket.io")
    });

    // we wait until we have a valid clientId, then start the MainScene
    socket.on('clientId', (clientId, onlinePlayersCount, onFieldPlayersCount) => {
      socket.clientId = clientId
      console.log('Your client id', clientId)
      this.scene.start('MenuScene', { socket, onlinePlayersCount, onFieldPlayersCount })
    });
  }
};

export default PreloadScene;
