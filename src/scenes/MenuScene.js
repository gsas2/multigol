import { Scene } from 'phaser';

class MenuScene extends Scene {
  constructor() {
    super({ key: 'MenuScene' })
  }

  init({ socket, onlinePlayersCount = 1, onFieldPlayersCount = 0}) {
    this.socket = socket;
    this.onlinePlayersCount = onlinePlayersCount;
    this.onFieldPlayersCount = onFieldPlayersCount;
  }

  create() {
    const TitleStyles = {
      color: '#ffffff',
      align: 'center',
      fontSize: 64
    };

    const MenuItemStyles = {
      color: '#ffffff',
      align: 'center',
      fontSize: 64
    };

    let texts = [];

    texts.push(this.add.text(10, 10, 'Bienvenido a MultiGol!', TitleStyles).setOrigin(0, 0));

    texts.push(
      this.add
        .text(
          this.sys.game.config.width / 2 - 100,
          200,
          'Jugar',
          MenuItemStyles
        )
        .setOrigin(0, 0)
        .setInteractive()
        .on('pointerdown', () => {
          this.socket.emit('requestFieldAccess');
        })
    );

    this.add.text(10, 80, 'Jugadores Online: ', { fontSize: '32px', fill: '#ffffff' }),
    this.add.text(420, 80, '/ En la cancha: ', { fontSize: '32px', fill: '#ffffff' }),
    this.usersInfo = [
      this.add.text(
        370,
        80,
        this.onlinePlayersCount,
        { fontSize: '32px', fill: '#ffffff' }
      ),
      this.add.text(
        740,
        80,
        this.onFieldPlayersCount,
        { fontSize: '32px', fill: '#ffffff' })
    ];

    this.socket.on('fieldAccessGranted', (gamerConfig) => {
      this.scene.stop();
      this.scene.start('FieldScene', { socket: this.socket, gamerConfig });
    });

    this.socket.on('onlinePlayers', (onlinePlayersCount, onFieldPlayersCount) => {
      this.onlinePlayersCount = onlinePlayersCount;
      this.onFieldPlayersCount = onFieldPlayersCount;
      this.usersInfo[0].setText(this.onlinePlayersCount);
      this.usersInfo[1].setText(this.onFieldPlayersCount);
    });

    // texts.push(
    //   this.add
    //     .text(0, 0, 'Arcade Physics (Level 1)', styles)
    //     .setOrigin(0.5, 0)
    //     .setInteractive()
    //     .on('pointerdown', () => {
    //       this.scene.start('MainScene', { scene: 'ArcadeScene', level: 0, socket: this.socket })
    //     })
    // )

    // texts.push(
    //   this.add
    //     .text(0, 0, 'Arcade Physics (Level 2)', styles)
    //     .setOrigin(0.5, 0)
    //     .setInteractive()
    //     .on('pointerdown', () => {
    //       this.scene.stop()
    //       this.scene.start('MainScene', { scene: 'ArcadeScene', level: 1, socket: this.socket })
    //     })
    // )

    // texts.push(
    //   this.add
    //     .text(0, 0, 'Arcade Physics (Level 3)', styles)
    //     .setOrigin(0.5, 0)
    //     .setInteractive()
    //     .on('pointerdown', () => {
    //       this.scene.stop()
    //       this.scene.start('MainScene', { scene: 'ArcadeScene', level: 2, socket: this.socket })
    //     })
    // )

    // const resize = () => {
    //   const { centerX, centerY } = this.cameras.main
    //   let posY = [20, centerY - 100, centerY - 10, centerY + 60, centerY + 130]
    //   texts.forEach((text, i) => {
    //     text.setPosition(centerX, posY[i])
    //   })
    // }

    // this.scale.on('resize', (gameSize: any, baseSize: any, displaySize: any, resolution: any) => {
    //   if (!this.scene.isActive()) return
    //   this.cameras.resize(gameSize.width, gameSize.height)
    //   resize()
    // })
    // resize()
    // Resize(this.game)
  }
}

export default MenuScene;
