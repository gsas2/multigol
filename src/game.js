import Phaser from 'phaser';

import PreloadScene from './scenes/PreloadScene';
import MenuScene from './scenes/MenuScene';
import FieldScene from './scenes/FieldScene';

const game = () => {
  var config = {
    width: 1860,
    height: 1020,
    type: Phaser.AUTO,
    backgroundColor: '#0f6b11',
    physics: {
      default: 'arcade',
      arcade: {
        // debug: true,
        gravity: { y: 0, x: 0 }
      }
    },
    scale: {
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH
    },
    scene: [PreloadScene, MenuScene, FieldScene]
  };

  return new Phaser.Game(config);
};

export default game;
