const express = require('express');
const app = express();
const server = require('http').Server(app);
require('./socket')(server);

const port = process.env.PORT || 8082;
server.listen(port, function () {
  console.log(`[SOCKET] Listening on ${server.address().port}`);
});
