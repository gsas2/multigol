const gameSocket = (server) => {
  const io = require('socket.io').listen(server);
  const onlineUsers = {};
  const gamers = {};
  let nextPlayerSide = 0;
  let scores = [0, 0];
  const resetScores = () => {
    scores = [0, 0];
  }

  const throwTheBall = (timeout = 5000) => {
    setTimeout(
      () => {
        io.emit('throwTheBall', {
          scores,
          position: {
            x: 1860 / 2, 
            y: 0,
          },
          velocity: {
            x: 1 + Math.random() * 100,
            y: Math.random() * 1000 + 3000
          }
        });
      },
      timeout
    );
  };

  const addGamer = (socket) => {
     const gamerConfig = {
      teamSide: nextPlayerSide,
      gamerId: socket.id,
      controlledSticks: {
        goalkeeper: true,
        defense: true,
        middle: true,
        attack: true
      }
    };
    gamers[socket.id] = gamerConfig;
    resetScores();
    nextPlayerSide = nextPlayerSide === 0 ? 1 : 0;
    socket.emit('fieldAccessGranted', gamerConfig);

    // when a player moves, update the player data
    socket.on('playerMovement', (movementData) => {
      // emit a message to all players about the player that moved
      socket.broadcast.emit('playerMoved', movementData);
    });

    // when ball moves, update all gamers
    socket.on('ballUpdate', (data) => {
      // console.log('Ball Update!', data);
      socket.broadcast.emit('ballKicked', data);
    });

    // when ball moves, update all gamers
    socket.on('goalDetected', (data) => {
      console.log('Goal!!!', data);
      let goalForTeam;
      if (data.goalForMyTeam) {
        goalForTeam = data.myTeamSide;
      } else {
        goalForTeam = data.myTeamSide === 0 ? 1 : 0;
      }
      scores[goalForTeam] += 1;
      io.emit('goalConfirmed', {
        goalForTeam,
        scores
      });
      throwTheBall(10000);
    });


    socket.on('lastKicker', kicker => {
      console.log('La tiene', kicker.name);
      socket.broadcast.emit('lastKicker', kicker);
    });

    if (Object.keys(gamers).length === 2) {
      throwTheBall();
    }
  };

  const notifyOnlinePlayers = () => {
    io.emit(
      'onlinePlayers',
      Object.keys(onlineUsers).length,
      Object.keys(gamers).length
    );
  }

  io.on('connection', (socket) => {
    console.log('[SOCKET] a user connected', socket.id);
    onlineUsers[socket.id] = true;
    // Return the clientId
    console.log('[SOCKET] User accepted', socket.id);
    socket.emit(
      'clientId',
      socket.id,
      Object.keys(onlineUsers).length,
      Object.keys(gamers).length
    );
    notifyOnlinePlayers();

    socket.on('disconnect', () => {
      console.log('[SOCKET] user disconnected', socket.id);
      if (onlineUsers[socket.id]) {
        delete onlineUsers[socket.id];
      }

      // remove this player from our gamers object
      if (gamers[socket.id]) {
        nextPlayerSide = gamers[socket.id].teamSide;
        delete gamers[socket.id];

        // Let the other players know this player left the field
        io.emit(
          'playerLeftField',
          Object.keys(onlineUsers).length,
          Object.keys(gamers).length
        );
      }

      notifyOnlinePlayers();
    });

    socket.on('requestFieldAccess', () => {
      if (Object.keys(gamers).length >= 2) {
        console.log('[SOCKET] User REJECTED!');
        return;
      }

      addGamer(socket);
      notifyOnlinePlayers();
      console.log('Total Gamers: ', Object.keys(gamers).length);
    });    
  });
};

module.exports = gameSocket;
