const express = require('express');
const app = express();
const server = require('http').Server(app);
require('./server/socket')(server);

app.use(express.static(__dirname + '/dist'));
 
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/dist/index.html');
});

const port = process.env.PORT || 8081;
server.listen(port, function () {
  console.log(`Listening on ${server.address().port}`);
});
